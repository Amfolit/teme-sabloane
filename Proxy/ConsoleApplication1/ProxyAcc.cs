﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class ProxyAcc :IAccount
    {
        private PrivateAcc _account;
        private readonly string _password = "prajituri";

        public ProxyAcc()
        {
            _account = new PrivateAcc() { Bani = 0};
        }


        public void Withdrow(string pass,int sum)
        {
            if (pass.Equals(_password))
            {
                if (_account == null)
                {
                    _account = new PrivateAcc() { Bani = 0 };
                }
                _account.Withdrow(pass, sum);
            }
            else
            {
                Console.WriteLine("Parola nu e buna ");
            }
        }

        public void Deposit(string pass, int sum)
        {
            if (pass.Equals(_password))
            {
                _account.Deposit(pass,sum);
            }
            else
            {
                Console.WriteLine("Parola nu e buna ");
            }
        }

        public void Display(string pass)
        {
            if (pass.Equals(_password))
            {
                _account.Display(pass);
            }
            else
            {
                Console.WriteLine("Parola nu e buna ");
            }
        }
    }
}
