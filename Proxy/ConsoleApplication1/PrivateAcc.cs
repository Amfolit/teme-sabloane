﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class PrivateAcc : IAccount
    {
        public int Bani { get; set; }

        public void Withdrow(string pass, int sum)
        {
            Bani -= sum;
        }

        public void Deposit(string pass, int sum)
        {
            Bani += sum;
        }

        public void Display(string pass)
        {
            Console.WriteLine(Bani);
        }
    }
}
