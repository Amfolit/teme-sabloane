﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6._1
{
    public class CashRegisterPaper : CashRegister
    {
        public override Money CreateNewMoney()
        {
            return new PaperMoney();
        }

        public override bool isSharedValue(double sum)
        {
            return PaperMoney.IsSharedMoney(sum);
        }
    }
}
