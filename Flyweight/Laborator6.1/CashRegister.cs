﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6._1
{
    public abstract class CashRegister
    {
        private Dictionary<double, Money> _sharedMoneyMap;
        private Money _unsharedMoney;
        private double _totalMoney;
        public CashRegister()
        {
            _totalMoney = 0;
            _sharedMoneyMap = new Dictionary<double, Money>();
        }
        protected Money LookUp(double sum)
        {
            if (this.isSharedValue(sum))
            {
                Money aux;
                if (!_sharedMoneyMap.ContainsKey(sum))
                {
                    aux = CreateNewMoney();
                    _sharedMoneyMap.Add(sum, aux);
                }
                else
                {
                    aux = _sharedMoneyMap[sum];
                }
                return aux;
            }
            else
            {
                return _unsharedMoney;
            }
        }

        public void CashIn(double sum)
        {
            LookUp(sum);
            _totalMoney += sum;
        }
        public void CashOut(double sum)
        {
            if(_sharedMoneyMap.Remove(sum))
                _totalMoney -= sum;
        }
        public double GetTotalCash()
        {
            return _totalMoney;
        }
        public abstract Money CreateNewMoney();
        public abstract bool isSharedValue(double sum);

    }
}
