﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6._1
{
    public class CoinMoney : Money
    {
        public static bool IsSharedMoney(double sum)
        {
            List<double> list = new List<double>() { 1, 5, 10, 50 };
            return list.Contains(sum);

        }
        public override EMoneyType GetMoneyType()
        {
            return EMoneyType.Coin;
        }
    }
}
