﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6._1
{
    public class Cashier
    {
        private CashRegister _coinMoney;
        private CashRegister _cardMoney;
        private CashRegister _paperMoney;

        public Cashier()
        {
            _cardMoney = new CashRegisterCard();
            _coinMoney = new CashRegisterCoin();
            _paperMoney = new CashRegisterPaper();
        }

        public void CashIn(double sum, EMoneyType type)
        {
            switch (type)
            {
                case EMoneyType.Coin:
                    _coinMoney.CashIn(sum);
                    break;
                case EMoneyType.Paper:
                    _paperMoney.CashIn(sum);
                    break;
                case EMoneyType.Card:
                    _cardMoney.CashIn(sum);
                    break;
                default:
                    break;
            }
        }

        public void CashOut(double sum, EMoneyType type)
        {
            switch (type)
            {
                case EMoneyType.Coin:
                    _coinMoney.CashOut(sum);
                    break;
                case EMoneyType.Paper:
                    _paperMoney.CashOut(sum);
                    break;
                case EMoneyType.Card:
                    _cardMoney.CashOut(sum);
                    break;
                default:
                    break;
            }
        }

        public double GetTotalCash()
        {
            return _cardMoney.GetTotalCash() + (_coinMoney.GetTotalCash()/100) + _paperMoney.GetTotalCash();
        }
    }
}
