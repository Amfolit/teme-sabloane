﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6._1
{
    public enum EMoneyType { Coin, Paper, Card }

    public abstract class Money
    {
        public double TotalCasheValue{get;set;}

        public abstract EMoneyType GetMoneyType();
    }
}
