﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6._1
{
    public class PaperMoney : Money
    {
        public static bool IsSharedMoney(double sum)
        {
            List<double> list = new List<double>() { 1, 5, 10, 50, 100, 200, 500 };
            return list.Contains(sum);

        }
        public override EMoneyType GetMoneyType()
        {
            return EMoneyType.Paper;
        }

    }
}
