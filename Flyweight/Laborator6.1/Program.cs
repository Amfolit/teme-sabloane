﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6._1
{

    public class Program
    {
        static void Main(string[] args)
        {
            Cashier cas = new Cashier();

            cas.CashIn(10, EMoneyType.Coin);

            cas.CashIn(10, EMoneyType.Paper);
            cas.CashIn(10, EMoneyType.Paper);
            cas.CashIn(100, EMoneyType.Paper);
            cas.CashIn(132, EMoneyType.Card);
            cas.CashIn(500, EMoneyType.Paper);

            cas.CashOut(132, EMoneyType.Card);
            Console.WriteLine(   cas.GetTotalCash());

        }
    }
}
