﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator3
{
    public enum Color { Red,Blue,Green}
    public enum Chasis { Big, Small, Flat }
    public enum Transmision { Manual, Automated }
    public enum Engine { Big, Fast, Lazy }
    public enum Direction { Left, Right }
    public enum CarType { Sedan, Suv, Crossover, HB, Pickup }
    public enum AccesoriesLow { SeatBelt, Airbags_face, ABS }
    public enum AccesoriesMediun { ESP, ComputerBoard, NavSistem, Airbags_sideways, AC, ElectricGeams }
    public enum AccesoriesHigh { ParkingSensor, AutomatedParking, HeatedSeats, HeatedParbriz, HeatedStearingWeels }

    public enum PriceLevel { Low, Medium, High }

    public class Car :ICarPrototype
    {
        public Color color { get; set; }
        public int wight { get; set; }
        public Chasis chasis { get; set; }
        public Transmision transmision { get; set; }
        public Engine engine { get; set; }
        public Direction direction { get; set; }
        public List<object> accesoriesList { get; set; }
        public CarType carType { get; set; }

        public Boolean IsClone;

        public Car()
        {
            accesoriesList = new List<object>();
            IsClone = false;
        }

        public override string ToString()
        {
            string desc = "Color = " + color + " " + ";Wight = " + wight + " " + ";Chasis = " + chasis +
                " " + ";Transmision = " + transmision + " " + ";Engine = " + engine + " " + ";Direction = " +
                direction + " " + ";CarType = " + carType;
            desc = desc + Environment.NewLine;

            return desc;
        }

        public Car Clone()
        {
            Car clone = new Car();
            clone = (Car)MemberwiseClone();
            if (this.color != (Color)0)
            {
                clone.color = this.color - 1;
            }
            else
                clone.color = Color.Green;

            clone.IsClone = true;
            return clone;
        }
    }
}
