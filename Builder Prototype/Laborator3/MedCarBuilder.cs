﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator3
{
    public class MedCarBuilder : CarBuilder
    {
        public override void BuildAccesories()
        {
            car.accesoriesList.Add(AccesoriesLow.ABS);
            car.accesoriesList.Add(AccesoriesLow.Airbags_face);
            car.accesoriesList.Add(AccesoriesLow.SeatBelt);
            car.accesoriesList.Add(AccesoriesMediun.AC);
            car.accesoriesList.Add(AccesoriesMediun.Airbags_sideways);
            car.accesoriesList.Add(AccesoriesMediun.ComputerBoard);
            car.accesoriesList.Add(AccesoriesMediun.ElectricGeams);
            car.accesoriesList.Add(AccesoriesMediun.ESP);
            car.accesoriesList.Add(AccesoriesMediun.NavSistem);
        }
    }
}
