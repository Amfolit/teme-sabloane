﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator3
{
    public interface ICarPrototype
    {
        Car Clone();
    }
}
