﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator3
{
    public interface ICarBuilder
    {
        void BuildColor();
        void BuildChasis();
        void BuildWeight();
        void BuildEngine();
        void BuildTransmision();
        void BuildDirection();
        void BuildAccesories();
        void BuildType();
        Car GetCar();
    }
}
