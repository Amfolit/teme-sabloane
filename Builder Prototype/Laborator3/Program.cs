﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator3
{
    class Program
    {
        static void Main(string[] args)
        {
            CarBuilderDirector cbd = new CarBuilderDirector(PriceLevel.Low);
            cbd.Construct();
            Car realCar = cbd.GetResult();
            Console.WriteLine(realCar.ToString());
            Console.WriteLine(realCar.IsClone);

            Car clone = realCar.Clone();

            Console.WriteLine(clone);
            Console.WriteLine(clone.IsClone);
        }
    }
}
