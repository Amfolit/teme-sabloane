﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator3
{
    public abstract class CarBuilder :ICarBuilder 
    {
        protected Car car = new Car();

        private Random rand = new Random();
        public void BuildColor()
        {
            car.color = (Color)rand.Next(0,2);
        }

        public void BuildChasis()
        {
            car.chasis = (Chasis)rand.Next(0,2);
        }

        public void BuildWeight()
        {
            car.wight = rand.Next(0,1000);
        }

        public void BuildEngine()
        {
            car.engine = (Engine)rand.Next(0,2);
        }

        public void BuildTransmision()
        {
            car.transmision = (Transmision)rand.Next(0,1);
        }

        public void BuildDirection()
        {
            car.direction = (Direction)rand.Next(0,1);
        }

        public abstract void BuildAccesories();

        public void BuildType()
        {
            car.carType = (CarType)rand.Next(0,4);
        }

        public Car GetCar()
        {
            return car;
        }
    }
}
