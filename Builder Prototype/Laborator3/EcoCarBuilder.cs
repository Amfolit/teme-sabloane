﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator3
{
    public class EcoCarBuilder : CarBuilder
    {
        public override void BuildAccesories()
        {
            car.accesoriesList.Add(AccesoriesLow.ABS);
            car.accesoriesList.Add(AccesoriesLow.Airbags_face);
            car.accesoriesList.Add(AccesoriesLow.SeatBelt);
        }
    }
}
