﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator3
{
    public class CarBuilderDirector
    {
        private ICarBuilder iCarBuilder;

        public CarBuilderDirector(PriceLevel priceLvl)
        {
            switch (priceLvl)
            {
                case PriceLevel.High:
                    iCarBuilder = new LuxCarBuilder();
                    break;
                case PriceLevel.Medium:
                    iCarBuilder = new MedCarBuilder();
                    break;
                case PriceLevel.Low:
                    iCarBuilder = new EcoCarBuilder();
                    break;
            }   
        }
        public void Construct()
        {
            iCarBuilder.BuildChasis();
            iCarBuilder.BuildColor();
            iCarBuilder.BuildDirection();
            iCarBuilder.BuildType();
            iCarBuilder.BuildWeight();
            iCarBuilder.BuildTransmision();
            iCarBuilder.BuildEngine();
            iCarBuilder.BuildAccesories();
        }

        public Car GetResult()
        {
            return iCarBuilder.GetCar();
        }
    }
}
