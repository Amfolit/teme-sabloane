﻿using Nasturii.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NasturiiCore
{
    public class SingletonAccount
    {
        public static object lockingObj = new object();
        private static SingletonAccount _instance;
        public ProxyAccount Account { get; set; }

        private SingletonAccount()
        {
           
        }

        public static SingletonAccount Instance
        {
            get
            {
                if (_instance == null)
                    lock (lockingObj)
                    {
                        if (_instance == null)
                        {
                            _instance = new SingletonAccount();
                        }
                    }
                return _instance;
            }
        }
    }
}
