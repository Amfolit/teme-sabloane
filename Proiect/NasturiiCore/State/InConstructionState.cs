﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NasturiiCore.State
{
    public class InConstructionState : State
    {
        public override bool CanBuild()
        {
            return false;
        }

        public override bool CanDecorate()
        {
            return false;
        }

        public override bool CanPrototype()
        {
            return true;
        }

        public override bool CanModify()
        {
            return true;
        }
    }
}
