﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NasturiiCore.State
{
    public abstract class State
    {
        public abstract bool CanDecorate();
        public abstract bool CanPrototype();
        public abstract bool CanBuild();
        public abstract bool CanModify();

        public override string ToString()
        {
            List<string> aux = this.GetType().ToString().Split('.').ToList();
            return aux[aux.Count-1];
        }
    }
}
