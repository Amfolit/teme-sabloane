﻿using Nasturii.BuilderClasses;
using Nasturii.Decorator;
using Nasturii.Prototype;
using NasturiiCore.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nasturii
{
    public enum EMaterial { Wood, Metal, Plastic, NA }
    public class Button : IPrototype
    {
        public string Color { get; set; }
        public int NrOfHole { get; set; }
        public EMaterial MaterialUsed { get; set; }
        public Shape Shape { get; set; }
        public bool isClone { get; set; }
        public List<EButtonType> ComponentList { get; set; }

        private volatile State _currentState = null;

        public State CurrentState {
            get { return _currentState; }
            set { _currentState = value; } }

        public Button()
        {
            isClone = false;
            ComponentList = new List<EButtonType>();
            _currentState = new IdleState();
        }

        public Button GetShallowClone()
        {
            Button aux = (Button)this.MemberwiseClone();
            aux.isClone = true;
            return aux;
        }

        public Button GetDeepClone()
        {
            Button deepClone = (Button)this.MemberwiseClone();

            deepClone.ComponentList = new List<EButtonType>();
            this.ComponentList.ForEach(a => deepClone.ComponentList.Add(a));
            deepClone.isClone = true;
            return deepClone;
        }


        public bool CanBuild()
        {
            return _currentState.CanBuild();
        }

        public bool CanPrototype()
        {
            return _currentState.CanPrototype();
        }

        public bool CanDecorate()
        {
            return _currentState.CanDecorate();
        }

        public bool CanModify()
        {
            return _currentState.CanModify();
        }

       


        public override string ToString()
        {
            return Color + " " + MaterialUsed + " " + NrOfHole + " " + Shape + " " + isClone;
        }
    }
}
