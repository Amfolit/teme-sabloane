﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.BuilderClasses
{
    public interface IButtonBuilder
    {
        void BuildColor(string color);
        void BuildHoles(int holes);
        void BuildMaterial(EMaterial material);
        void BuildShape();
        Button GetButton();
    }
}
