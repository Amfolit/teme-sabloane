﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.BuilderClasses
{
    public enum Shape { Round, Square , NA }
    public class ButtonDirector
    {
        private IButtonBuilder _builder;
        private Button _createdButton;

        public ButtonDirector(Shape shape)
        {
            switch (shape)
            {
                case Shape.Square:
                    _builder = new SquareBuilder();
                    break;
                case Shape.Round:
                    _builder = new RoundBuilder();
                    break;
                default:
                    break;
            }
        }

        public void Build(String color,int holes,EMaterial material)
        {
            _builder.BuildColor(color);
            _builder.BuildHoles(holes);
            _builder.BuildMaterial(material);
            _builder.BuildShape();
        }
        public Button GetResult()
        {
            return _builder.GetButton();
        }
    }
}
