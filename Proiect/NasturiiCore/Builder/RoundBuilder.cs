﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.BuilderClasses
{
    public class RoundBuilder : ButtonBuilder
    {
        public override void BuildShape()
        {
            button.Shape = Shape.Round;
        }
    }
}
