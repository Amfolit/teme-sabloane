﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.BuilderClasses
{
    public class SquareBuilder : ButtonBuilder
    {
        public override void BuildShape()
        {
            button.Shape = Shape.Square;
        }
    }
}
