﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.BuilderClasses
{
    public abstract class ButtonBuilder : IButtonBuilder
    {
        protected Button button = new Button();
        public abstract void BuildShape();

        public void BuildColor(string color)
        {
            button.Color = color;
        }

        public void BuildHoles(int holes)
        {
            button.NrOfHole = holes;
        }

        public void BuildMaterial(EMaterial material)
        {
            button.MaterialUsed = material;
        }

        public Button GetButton()
        {
            return button;
        }
    }
}
