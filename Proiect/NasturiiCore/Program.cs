﻿using Nasturii.BuilderClasses;
using Nasturii.Decorator;
using Nasturii.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii
{
    class Program
    {
        static void Main(string[] args)
        {
            ProxyAccount acc = new ProxyAccount("","");

            acc.Build(Shape.Square , 5 , "fucsia", EMaterial.Wood);

            Button butt = acc.GetResult();
            Console.WriteLine(butt);

            Button clone = acc.GetShallowClone(butt);
            Console.WriteLine(clone);

            acc.Decorate(clone,EButtonType.Spotted);

            Console.WriteLine(clone.ComponentList.First());
        }
    }
}
