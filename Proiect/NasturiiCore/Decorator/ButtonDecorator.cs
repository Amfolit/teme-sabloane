﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.Decorator
{
    public abstract class ButtonDecorator : IButton
    {
        protected Button button;

        public ButtonDecorator(Button butt)
        {
            button = butt;
        }

        virtual public void Decorate(){}
    }
}
