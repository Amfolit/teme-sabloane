﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.Decorator
{
    public enum EButtonType { Striped, Spotted }
    public interface IButton
    {
        void Decorate();
    }
}
