﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.Decorator
{
    public class SpottedDecorator : ButtonDecorator
    {
        public SpottedDecorator(Button butt)
            :base(butt)
        {

        }
        public override void Decorate()
        {
            button.ComponentList.Add(EButtonType.Spotted);
        }
    }
}
