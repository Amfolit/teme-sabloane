﻿using Nasturii.BuilderClasses;
using Nasturii.Decorator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.Proxy
{
    public class ProxyAccount : IAccount
    {
        private PrivateAccount _account;
        private readonly string _password = "Mucegai";

        public ProxyAccount(string pass)
        {
            _account = null;
            if (pass.Equals(_password))
            {
                _account = new PrivateAccount();
            }
            else
            {
                Console.WriteLine("Wrong password");
            }
        }

        public void Build(Shape shape, int holes, string color, EMaterial mat)
        {
            if (_account != null)
            {
                _account.Build(shape, holes, color, mat);
            }
        }

        public Button GetResult()
        {
            if (_account != null)
                return _account.GetResult();
            return null;
        }

        public Button GetShallowClone(Button button)
        {
            if (_account != null)
                return _account.GetShallowClone(button);
            return null;
        }

        public Button GetDeepClone(Button button)
        {
            if (_account != null)
                return _account.GetDeepClone(button);
            return null;
        }

        public void Decorate(Button button, EButtonType buttonType)
        {
            if (_account != null)
                _account.Decorate(button,buttonType);
        }

    }
}
