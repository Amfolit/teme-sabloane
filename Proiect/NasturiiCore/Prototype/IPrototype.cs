﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.Prototype
{
    public interface IPrototype
    {
        Button GetShallowClone();
        Button GetDeepClone();
    }
}
