﻿using Nasturii.BuilderClasses;
using Nasturii.Decorator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.Proxy
{
    public interface IAccount
    {
        void Build(Shape shape, int holes, string color, EMaterial mat);
        Button GetResult();
        Button GetShallowClone(Button button);
        Button GetDeepClone(Button button);
        void Decorate(Button button, EButtonType buttonType);
    }
}
