﻿using Nasturii.BuilderClasses;
using Nasturii.Decorator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.Proxy
{
    public class ProxyAccount : IAccount
    {
        private PrivateAccount _account;
        private readonly Dictionary<string, string> _accounts
            = new Dictionary<string, string>() { { "", "" }, { "da", "da" } };

        public ProxyAccount(string user, string pass)
        {
            _account = null;

            if(_accounts.ContainsKey(user))
            {
                if(_accounts[user].Equals(pass))
                {
                    _account = new PrivateAccount();
                }
            }
        }

        public void Build(Shape shape, int holes, string color, EMaterial mat)
        {
            if (_account != null)
            {
                _account.Build(shape, holes, color, mat);
            }
        }

        public Button GetResult()
        {
            if (_account != null)
                return _account.GetResult();
            return null;
        }

        public Button GetShallowClone(Button button)
        {
            if (_account != null)
                return _account.GetShallowClone(button);
            return null;
        }

        public Button GetDeepClone(Button button)
        {
            if (_account != null)
                return _account.GetDeepClone(button);
            return null;
        }

        public void Decorate(Button button, EButtonType buttonType)
        {
            if (_account != null)
                _account.Decorate(button, buttonType);
        }

        public bool IsValid()
        {
            if (_account == null)
                return false;
            return true;
        }
    }
}
