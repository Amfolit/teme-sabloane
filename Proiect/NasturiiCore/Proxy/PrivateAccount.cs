﻿using Nasturii.BuilderClasses;
using Nasturii.Decorator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasturii.Proxy
{
    public class PrivateAccount : IAccount
    {
        private ButtonDirector _bd;
        private IButton _decorator;
        public PrivateAccount()
        {
            _bd = null;
            _decorator = null;
        }

        public void Build(Shape shape,int holes,string color,EMaterial mat)
        {
            _bd = new ButtonDirector(shape);
            _bd.Build(color,holes,mat);
        }

        public Button GetResult()
        {
            if (_bd != null)
            {
                return _bd.GetResult();
            }
            return null;
        }

        public Button GetShallowClone(Button button)
        {
            return button.GetShallowClone();
        }

        public Button GetDeepClone(Button button)
        {
            return button.GetDeepClone();
        }

        public void Decorate(Button button, EButtonType buttonType)
        {
            if (buttonType == EButtonType.Spotted)
            {
                _decorator = new SpottedDecorator(button);
                _decorator.Decorate();
            }
            else if (buttonType == EButtonType.Striped)
            {
                _decorator = new StripedDecorator(button);
                _decorator.Decorate();
            }

        }
    }
}
