﻿using Nasturii.Proxy;
using NasturiiCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NasturiIsTari
{
    public partial class ButtonWindow : Form
    {
        public ButtonWindow()
        {
            InitializeComponent();
            this.AcceptButton = loginButton;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            var username = userBox.Text;
            var password = passwordBox.Text;
            ProxyAccount account = new ProxyAccount(username, password);
            if (account.IsValid())
            {
                SingletonAccount.Instance.Account = account;
                Fabrica fabricaView = new Fabrica();
                fabricaView.ShowDialog();
            }
            else
            {
                MessageBox.Show("Wrong Credentials");
            }
        }

        private void userNameLabel_Click(object sender, EventArgs e)
        {

        }
    }
}

