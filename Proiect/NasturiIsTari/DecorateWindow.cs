﻿using Nasturii.Decorator;
using Nasturii.Proxy;
using NasturiiCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NasturiIsTari
{
    public partial class DecorateWindow : Form
    {
        Nasturii.Button butt;
        public DecorateWindow(Nasturii.Button button)
        {
            InitializeComponent();
            butt = button;
        }

        private void decorateButton_Click(object sender, EventArgs e)
        {
            if (decorateBox.Text.Equals("Striped"))
            {
                SingletonAccount.Instance.Account.Decorate(butt, EButtonType.Striped);
            }
            else if (decorateBox.Text.Equals("Spotted"))
            {
                SingletonAccount.Instance.Account.Decorate(butt, EButtonType.Spotted);
            }
            MessageBox.Show("Button has beed decorated with " + decorateBox.Text);
            this.DialogResult = DialogResult.OK;
        }
    }
}
