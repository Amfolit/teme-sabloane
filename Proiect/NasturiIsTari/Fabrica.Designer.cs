﻿namespace NasturiIsTari
{
    partial class Fabrica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MaterialCombo = new System.Windows.Forms.ComboBox();
            this.gauriBox = new System.Windows.Forms.TextBox();
            this.shapeCombo = new System.Windows.Forms.ComboBox();
            this.colorBox = new System.Windows.Forms.TextBox();
            this.ColorLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BuildButton = new System.Windows.Forms.Button();
            this.grid = new System.Windows.Forms.DataGridView();
            this.DecorateButton = new System.Windows.Forms.Button();
            this.constructButton = new System.Windows.Forms.Button();
            this.PrototypeButton = new System.Windows.Forms.Button();
            this.modityButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // MaterialCombo
            // 
            this.MaterialCombo.FormattingEnabled = true;
            this.MaterialCombo.Items.AddRange(new object[] {
            "Wood",
            "Metal",
            "Plastic"});
            this.MaterialCombo.Location = new System.Drawing.Point(70, 75);
            this.MaterialCombo.Name = "MaterialCombo";
            this.MaterialCombo.Size = new System.Drawing.Size(121, 21);
            this.MaterialCombo.TabIndex = 0;
            // 
            // gauriBox
            // 
            this.gauriBox.Location = new System.Drawing.Point(70, 113);
            this.gauriBox.Name = "gauriBox";
            this.gauriBox.Size = new System.Drawing.Size(121, 20);
            this.gauriBox.TabIndex = 1;
            // 
            // shapeCombo
            // 
            this.shapeCombo.FormattingEnabled = true;
            this.shapeCombo.Items.AddRange(new object[] {
            "Round",
            "Square"});
            this.shapeCombo.Location = new System.Drawing.Point(70, 154);
            this.shapeCombo.Name = "shapeCombo";
            this.shapeCombo.Size = new System.Drawing.Size(121, 21);
            this.shapeCombo.TabIndex = 2;
            // 
            // colorBox
            // 
            this.colorBox.Location = new System.Drawing.Point(70, 189);
            this.colorBox.Name = "colorBox";
            this.colorBox.Size = new System.Drawing.Size(121, 20);
            this.colorBox.TabIndex = 4;
            // 
            // ColorLabel
            // 
            this.ColorLabel.AutoSize = true;
            this.ColorLabel.Location = new System.Drawing.Point(12, 192);
            this.ColorLabel.Name = "ColorLabel";
            this.ColorLabel.Size = new System.Drawing.Size(37, 13);
            this.ColorLabel.TabIndex = 5;
            this.ColorLabel.Text = "Color :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nr Gauri :";
            // 
            // BuildButton
            // 
            this.BuildButton.Location = new System.Drawing.Point(84, 234);
            this.BuildButton.Name = "BuildButton";
            this.BuildButton.Size = new System.Drawing.Size(75, 23);
            this.BuildButton.TabIndex = 7;
            this.BuildButton.Text = "Build";
            this.BuildButton.UseVisualStyleBackColor = true;
            this.BuildButton.Click += new System.EventHandler(this.BuildButton_Click);
            // 
            // grid
            // 
            this.grid.AllowUserToAddRows = false;
            this.grid.AllowUserToDeleteRows = false;
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grid.Location = new System.Drawing.Point(204, 30);
            this.grid.MultiSelect = false;
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(560, 227);
            this.grid.TabIndex = 8;
            this.grid.SelectionChanged += new System.EventHandler(this.grid_SelectionChanged);
            // 
            // DecorateButton
            // 
            this.DecorateButton.Location = new System.Drawing.Point(234, 281);
            this.DecorateButton.Name = "DecorateButton";
            this.DecorateButton.Size = new System.Drawing.Size(75, 23);
            this.DecorateButton.TabIndex = 9;
            this.DecorateButton.Text = "Decorate";
            this.DecorateButton.UseVisualStyleBackColor = true;
            this.DecorateButton.Click += new System.EventHandler(this.DecorateButton_Click);
            // 
            // constructButton
            // 
            this.constructButton.Location = new System.Drawing.Point(560, 281);
            this.constructButton.Name = "constructButton";
            this.constructButton.Size = new System.Drawing.Size(75, 23);
            this.constructButton.TabIndex = 10;
            this.constructButton.Text = "Construct";
            this.constructButton.UseVisualStyleBackColor = true;
            this.constructButton.Click += new System.EventHandler(this.constructButton_Click);
            // 
            // PrototypeButton
            // 
            this.PrototypeButton.Location = new System.Drawing.Point(671, 281);
            this.PrototypeButton.Name = "PrototypeButton";
            this.PrototypeButton.Size = new System.Drawing.Size(75, 23);
            this.PrototypeButton.TabIndex = 11;
            this.PrototypeButton.Text = "Prototype";
            this.PrototypeButton.UseVisualStyleBackColor = true;
            this.PrototypeButton.Click += new System.EventHandler(this.Prototype_Click);
            // 
            // modityButton
            // 
            this.modityButton.Location = new System.Drawing.Point(354, 281);
            this.modityButton.Name = "modityButton";
            this.modityButton.Size = new System.Drawing.Size(75, 23);
            this.modityButton.TabIndex = 12;
            this.modityButton.Text = "Modify";
            this.modityButton.UseVisualStyleBackColor = true;
            this.modityButton.Click += new System.EventHandler(this.modityButton_Click);
            // 
            // Fabrica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 327);
            this.Controls.Add(this.modityButton);
            this.Controls.Add(this.PrototypeButton);
            this.Controls.Add(this.constructButton);
            this.Controls.Add(this.DecorateButton);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.BuildButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ColorLabel);
            this.Controls.Add(this.colorBox);
            this.Controls.Add(this.shapeCombo);
            this.Controls.Add(this.gauriBox);
            this.Controls.Add(this.MaterialCombo);
            this.Name = "Fabrica";
            this.Text = "Fabrica";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Close);
            this.Load += new System.EventHandler(this.Fabrica_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox MaterialCombo;
        private System.Windows.Forms.TextBox gauriBox;
        private System.Windows.Forms.ComboBox shapeCombo;
        private System.Windows.Forms.TextBox colorBox;
        private System.Windows.Forms.Label ColorLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BuildButton;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.Button DecorateButton;
        private System.Windows.Forms.Button constructButton;
        private System.Windows.Forms.Button PrototypeButton;
        private System.Windows.Forms.Button modityButton;
    }
}