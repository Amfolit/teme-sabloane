﻿using Nasturii;
using Nasturii.BuilderClasses;
using Nasturii.Proxy;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Specialized;
using NasturiiCore.State;
using System.Threading;
using NasturiiCore;

namespace NasturiIsTari
{
    public partial class Fabrica : Form
    {
        public ObservableCollection<Nasturii.Button> buttonList;

        public Fabrica()
        {
            InitializeComponent();
            shapeCombo.SelectedIndex = 0;
            MaterialCombo.SelectedIndex = 0;
            buttonList = new ObservableCollection<Nasturii.Button>();
            grid.DataSource = new BindingSource { DataSource = buttonList };
        }

        private void Fabrica_Load(object sender, EventArgs e)
        {
        }

        private void Close(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
        

        private void BuildButton_Click(object sender, EventArgs e)
        {
            int nrGauri = 0;
            Shape shape = Shape.NA;
            EMaterial mat = EMaterial.NA;

            Int32.TryParse(gauriBox.Text, out nrGauri);

            if (shapeCombo.SelectedItem.ToString().Equals("Square"))
                shape = Shape.Square;
            else if (shapeCombo.SelectedItem.ToString().Equals("Round"))
                shape = Shape.Round;

            if (MaterialCombo.SelectedItem.ToString().Equals("Wood"))
                mat = EMaterial.Wood;
            else if (MaterialCombo.SelectedItem.ToString().Equals("Plastic"))
                mat = EMaterial.Plastic;
            else if (MaterialCombo.SelectedItem.ToString().Equals("Metal"))
                mat = EMaterial.Metal;


            SingletonAccount.Instance.Account.Build(shape, nrGauri, colorBox.Text, mat);

            buttonList.Add(SingletonAccount.Instance.Account.GetResult());
            grid.DataSource = new BindingSource { DataSource = buttonList };

            ResetBoxes();
        }

        private void ResetBoxes()
        {
            colorBox.Text = "";
            gauriBox.Text = "";
            MaterialCombo.SelectedIndex = 0;
            shapeCombo.SelectedIndex = 0;
        }
        
        private void grid_SelectionChanged(object sender, EventArgs e)
        {
            Nasturii.Button button = buttonList[grid.CurrentCell.RowIndex];
            RefreshButton(button);
        }


        private void RefreshButton(Nasturii.Button button)
        {
            if (button.CanDecorate())
            {
                DecorateButton.Enabled = true;
            }
            else
            {
                DecorateButton.Enabled = false;
            }

            if (button.CanBuild())
            {
                constructButton.Enabled = true;
            }
            else
            {
                constructButton.Enabled = false;
            }
            if (button.CanPrototype())
            {
                PrototypeButton.Enabled = true;
            }
            else
            {
                PrototypeButton.Enabled = false;
            }
            if (button.CanModify())
            {
                modityButton.Enabled = true;
            }
            else
            {
                modityButton.Enabled = false;
            }
        }

        private void StartConstruction(Nasturii.Button button)
        {
            button.CurrentState = new InConstructionState();
            MessageBox.Show("Constructing Button");
            Thread.Sleep(200);
            button.CurrentState = new DoneState();
            Application.DoEvents();
            RefreshButton(button);
        }


        private void CallCommand(string command)
        {
            if (command.Equals("construct"))
            {
                Nasturii.Button button = buttonList[grid.CurrentCell.RowIndex];
                StartConstruction(button);
            }
            else if (command.Equals("prototype"))
            {
                MessageBox.Show(SingletonAccount.Instance.Account.GetResult().ToString());
            }
            else if (command.Equals("modify"))
            {
                Nasturii.Button button = buttonList[grid.CurrentCell.RowIndex];
                ModifyWindow fabricaView = new ModifyWindow(button);
                fabricaView.ShowDialog();
            }
            else if (command.Equals("decorate"))
            {
                Nasturii.Button button = buttonList[grid.CurrentCell.RowIndex];
                DecorateWindow fabricaView = new DecorateWindow(button);
                fabricaView.ShowDialog();
            }
        }


        private void constructButton_Click(object sender, EventArgs e)
        {
            CallCommand("construct");
        }

        private void Prototype_Click(object sender, EventArgs e)
        {
            CallCommand("prototype");
        }

        private void modityButton_Click(object sender, EventArgs e)
        {
            CallCommand("modify");
        }

        private void DecorateButton_Click(object sender, EventArgs e)
        {
            CallCommand("decorate");
        }
    }
}
