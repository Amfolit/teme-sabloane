﻿using Nasturii;
using Nasturii.BuilderClasses;
using Nasturii.Proxy;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Specialized;

namespace NasturiIsTari
{
    public partial class ModifyWindow : Form
    {
        Nasturii.Button button;
        public ModifyWindow(Nasturii.Button button)
        {
            InitializeComponent();
            this.button = button;
        }
        public ModifyWindow()
        {
            gauriBox.Text = button.NrOfHole.ToString();
            colorBox.Text = button.Color;
            MaterialCombo.SelectedIndex = 0;
            shapeCombo.SelectedIndex = 0;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            
        }

        private void ModifyButton_Click(object sender, EventArgs e)
        {
            int holes = 0;
            Int32.TryParse(gauriBox.Text, out holes);
            button.Color = colorBox.Text;
            button.NrOfHole = holes;
            if (shapeCombo.SelectedItem.ToString().Equals("Square"))
                button.Shape = Shape.Square;
            else if (shapeCombo.SelectedItem.ToString().Equals("Round"))
                button.Shape = Shape.Round;

            if (MaterialCombo.SelectedItem.ToString().Equals("Wood"))
                button.MaterialUsed = EMaterial.Wood;
            else if (MaterialCombo.SelectedItem.ToString().Equals("Plastic"))
                button.MaterialUsed = EMaterial.Plastic;
            else if (MaterialCombo.SelectedItem.ToString().Equals("Metal"))
                button.MaterialUsed = EMaterial.Metal;
            this.DialogResult = DialogResult.OK;
        }
        private void Close(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
        private void ResetBoxes()
        {
            colorBox.Text = "";
            gauriBox.Text = "";
            MaterialCombo.SelectedIndex = 0;
            shapeCombo.SelectedIndex = 0;
        }

    }
}
