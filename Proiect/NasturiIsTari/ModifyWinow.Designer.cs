﻿namespace NasturiIsTari
{
    partial class ModifyWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MaterialCombo = new System.Windows.Forms.ComboBox();
            this.shapeCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.colorBox = new System.Windows.Forms.TextBox();
            this.gauriBox = new System.Windows.Forms.TextBox();
            this.ModifyButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MaterialCombo
            // 
            this.MaterialCombo.FormattingEnabled = true;
            this.MaterialCombo.Items.AddRange(new object[] {
            "Wood",
            "Metal",
            "Plastic"});
            this.MaterialCombo.Location = new System.Drawing.Point(79, 47);
            this.MaterialCombo.Name = "MaterialCombo";
            this.MaterialCombo.Size = new System.Drawing.Size(121, 21);
            this.MaterialCombo.TabIndex = 0;
            // 
            // shapeCombo
            // 
            this.shapeCombo.FormattingEnabled = true;
            this.shapeCombo.Items.AddRange(new object[] {
            "Round",
            "Square"});
            this.shapeCombo.Location = new System.Drawing.Point(79, 113);
            this.shapeCombo.Name = "shapeCombo";
            this.shapeCombo.Size = new System.Drawing.Size(121, 21);
            this.shapeCombo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Color";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nr. Gauri:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // colorBox
            // 
            this.colorBox.Location = new System.Drawing.Point(79, 146);
            this.colorBox.Name = "colorBox";
            this.colorBox.Size = new System.Drawing.Size(100, 20);
            this.colorBox.TabIndex = 4;
            // 
            // gauriBox
            // 
            this.gauriBox.Location = new System.Drawing.Point(79, 78);
            this.gauriBox.Name = "gauriBox";
            this.gauriBox.Size = new System.Drawing.Size(100, 20);
            this.gauriBox.TabIndex = 5;
            // 
            // ModifyButton
            // 
            this.ModifyButton.Location = new System.Drawing.Point(79, 192);
            this.ModifyButton.Name = "ModifyButton";
            this.ModifyButton.Size = new System.Drawing.Size(75, 23);
            this.ModifyButton.TabIndex = 6;
            this.ModifyButton.Text = "Modify";
            this.ModifyButton.UseVisualStyleBackColor = true;
            this.ModifyButton.Click += new System.EventHandler(this.ModifyButton_Click);
            // 
            // ModifyWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 300);
            this.Controls.Add(this.ModifyButton);
            this.Controls.Add(this.gauriBox);
            this.Controls.Add(this.colorBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.shapeCombo);
            this.Controls.Add(this.MaterialCombo);
            this.Name = "ModifyWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox MaterialCombo;
        private System.Windows.Forms.ComboBox shapeCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox colorBox;
        private System.Windows.Forms.TextBox gauriBox;
        private System.Windows.Forms.Button ModifyButton;
    }
}