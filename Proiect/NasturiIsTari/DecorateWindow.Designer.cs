﻿namespace NasturiIsTari
{
    partial class DecorateWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.decorateBox = new System.Windows.Forms.ComboBox();
            this.decorateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // decorateBox
            // 
            this.decorateBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Striped",
            "Spotted"});
            this.decorateBox.FormattingEnabled = true;
            this.decorateBox.Items.AddRange(new object[] {
            "Spotted",
            "Striped"});
            this.decorateBox.Location = new System.Drawing.Point(45, 44);
            this.decorateBox.Name = "decorateBox";
            this.decorateBox.Size = new System.Drawing.Size(121, 21);
            this.decorateBox.TabIndex = 0;
            // 
            // decorateButton
            // 
            this.decorateButton.Location = new System.Drawing.Point(61, 110);
            this.decorateButton.Name = "decorateButton";
            this.decorateButton.Size = new System.Drawing.Size(75, 23);
            this.decorateButton.TabIndex = 1;
            this.decorateButton.Text = "Decorate";
            this.decorateButton.UseVisualStyleBackColor = true;
            this.decorateButton.Click += new System.EventHandler(this.decorateButton_Click);
            // 
            // DecorateWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(211, 188);
            this.Controls.Add(this.decorateButton);
            this.Controls.Add(this.decorateBox);
            this.Name = "DecorateWindow";
            this.Text = "DecorateWindow";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox decorateBox;
        private System.Windows.Forms.Button decorateButton;
    }
}