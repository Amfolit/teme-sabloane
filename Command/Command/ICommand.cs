﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    public interface ICommand
    {
        char ArithOperator { get; set; }
        double Operand { get; set; }

        char RedoCommandSign();
        void Execute();
        void ExecuteReverse();
    }
}
