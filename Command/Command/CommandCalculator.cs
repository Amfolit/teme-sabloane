﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    public class CommandCalculator : ICommand
    {
        private Calculator _calculator;

        private char _arith;
        private double _operand;

        public CommandCalculator()
        {
            _calculator = new Calculator();
        }

        public void Execute()
        {
            _calculator.DoOperation(ArithOperator,Operand);
        }

        public void ExecuteReverse()
        {
            _calculator.DoOperation(RedoCommandSign(), Operand);
        }

        public char RedoCommandSign()
        {
            switch (_arith)
            {
                case '+':
                    return '-';
                case '-':
                    return '+';
                case '*':
                    return '/';
                case '/':
                    return '*';
            }
            return ' ';
        }

        public char ArithOperator
        {
            get
            {
                return _arith;
            }
            set
            {
                _arith = value;
            }
        }

        public double Operand
        {
            get
            {
                return _operand;
            }
            set
            {
                _operand = value;
            }
        }
    }
}
