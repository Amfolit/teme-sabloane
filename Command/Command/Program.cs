﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    public class Program
    {
        static void Main(string[] args)
        {
            CalculatorInvoker ci = new CalculatorInvoker();

            ci.Undo();
            ci.Compute('+', 25.5);
            ci.Compute('+', 22.5);
            ci.Undo();
            ci.Redo();
            ci.Compute('*', 3);


        }
    }
}
