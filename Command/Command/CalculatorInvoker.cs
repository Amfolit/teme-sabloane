﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    public class CalculatorInvoker
    {
        public ICommand _command { get; set; }

        public Stack<ICommand> undoQue;
        public Stack<ICommand> redoQue;


        public CalculatorInvoker()
        {
            _command = new CommandCalculator();
            undoQue = new Stack<ICommand>();
            redoQue = new Stack<ICommand>();
        }

        public void Compute(char sign, double value)
        {
            _command.ArithOperator = sign;
            _command.Operand = value;
            _command.Execute();
            undoQue.Push(_command);
        }

        public void Undo()
        {
            if (undoQue.Count != 0)
            {
                ICommand operationToRedo = undoQue.Pop();
                operationToRedo.ExecuteReverse();
                redoQue.Push(operationToRedo);
            }
            else
                Console.WriteLine("Nothing to undo");
        }

        public void Redo()
        {
            if (redoQue.Count != 0)
            {
                ICommand operationToRedo = redoQue.Pop();
                operationToRedo.Execute();
            }
            else
                Console.WriteLine("Nothing to redo");
        }
    }
}
