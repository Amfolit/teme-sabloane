﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    public class Calculator
    {
        private double _currentValue;

        public Calculator()
        {
            _currentValue = 0;
        }
        public void DoOperation(char sign, double value)
        {
            switch (sign)
            {
                case '+':
                    _currentValue += value;
                    break;
                case '-':
                    _currentValue -= value;
                    break;
                case '*':
                    _currentValue *= value;
                    break;
                case '/':
                    _currentValue /= value;
                    break;
            }
            Console.WriteLine("Operation = " + sign +" value = " + value);
            Console.WriteLine("New Value = "+ _currentValue);
        }
    }
}
