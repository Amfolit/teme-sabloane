﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6
{
    class Program
    {
        static void Main(string[] args)
        {
            CylindricalPlug cp = new CylindricalPlug("red", "blue");

            Console.WriteLine(cp.GetPowerSupply(EAdapterType.eClassAdapter));
            Console.WriteLine(cp.GetPowerSupply(EAdapterType.eObjectAdapter));
        }
    }
}
