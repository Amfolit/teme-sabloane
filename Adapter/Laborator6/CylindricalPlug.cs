﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator6
{
    public enum EAdapterType { eClassAdapter, eObjectAdapter }
    public class CylindricalPlug
    {
        private string _firstWire;
        private string _secondWire;

        public CylindricalPlug(string first, string second)
        {
            _firstWire = first;
            _secondWire = second;
        }

        public string GetPowerSupply(EAdapterType type)
        {
            switch (type)
            {
                case EAdapterType.eClassAdapter:
                    CylindricalObjectAdapter coa = new CylindricalObjectAdapter();
                    return coa.GetPowerSupply(_firstWire,_secondWire);
                case EAdapterType.eObjectAdapter:
                    CylindricalClassAdapter cca = new CylindricalClassAdapter();
                    return cca.GetPowerSupply(_firstWire,_secondWire);
                default:
                    return "no power";
            }
        }
    }
}
