﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator1
{
    public class Laborator1
    {
        static void Main(string[] args)
        {

            LoadBalancer inst = LoadBalancer.Instance;

            for (int i = 0; i < 5000; i++)
            {
                inst.ReciveRequest();
            }

            Tuple<int, int> tuple = inst.GetFastRaport();
            Console.WriteLine("_________________________________________________________");
            Console.WriteLine("Slowest timp response " + tuple.Item1 + "\n" + "Fastest time " + tuple.Item2);
            Console.WriteLine("Total Number of requests " + inst.NrOfRequest());
            Console.WriteLine("Total execution time " + inst.TotalExecutionTime());
            Console.WriteLine("Avrage execution time " + inst.AvgExecTimp());
            Console.WriteLine("_________________________________________________________");
        }
    }
}
