﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator1
{
    public class LoadBalancer
    {
        private static LoadBalancer _instance;
        public List<Server> serverList { get; set; }
        public Boolean flag = true;
        private Random rand = new Random();
        public static object lockingObj = new object();


        public static LoadBalancer Instance
        {
            get
            {
                if (_instance == null)
                    lock (lockingObj)
                    {
                        if (_instance == null)
                        {
                            _instance = new LoadBalancer();
                        }
                    }
                return _instance;
            }
        }

        private LoadBalancer()
        {
            serverList = new List<Server>()
            {
                new Server("server1","1.1.1.1"),
                new Server("server2","1.1.1.2"),
                new Server("server3","1.1.1.3"),
                new Server("server4","1.1.1.4"),
                new Server("server5","1.1.1.5"),
                new Server("server6","1.1.1.6"),
                new Server("server7","1.1.1.7"),
                new Server("server8","1.1.1.8"),
                new Server("server9","1.1.1.9"),
                new Server("server10","1.1.1.10")
            };
        }

        public void ReciveRequest()
        {
            if (flag)
            {
                int number = GetRandomNumber();
                serverList[number].ReciveRequest(rand.Next(500,1000));
            }
            else
            {
                Server aux = findFreeServer();
                aux.ReciveRequest(rand.Next(500, 1000));
            }
        }

        private int GetRandomNumber()
        {
            int da = rand.Next(0, 9);
            return da;
        }
        private Server findFreeServer()
        {
            Server aux = new Server();
            int min = 100;
            foreach (Server a in serverList)
            {
                if (a.NrOfRequest <= min)
                {
                    min = a.NrOfRequest;
                    aux = a;
                }
            }
            return aux;
        }
        public Tuple<int,int> GetFastRaport()
        {

            int slowTime = 1000;
            int fastTime = 500;
            foreach (Server server in serverList)
            {
                if (server.ExecutionTime < slowTime && server.ExecutionTime !=0 )
                    slowTime = server.ExecutionTime;
                if (server.ExecutionTime > fastTime)
                    fastTime = server.ExecutionTime;
            }
            return new Tuple<int,int>(slowTime, fastTime);
        }
        public int NrOfRequest()
        {
            int totalNumerOfRequests = 0;
            foreach (Server a in serverList)
                totalNumerOfRequests += a.NrOfRequest;
            return totalNumerOfRequests;
        }

        public int TotalExecutionTime()
        {
            int totalExecTime = 0;
            foreach (Server a in serverList)
                totalExecTime += a.TotalServerExecTime;
            return totalExecTime;
        }

        public int AvgExecTimp()
        {
            return TotalExecutionTime() / 50;
        }
    }
}
