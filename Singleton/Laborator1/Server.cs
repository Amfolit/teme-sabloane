﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator1
{
    public class Server
    {
        public string Name { get; set; }
        public string IpAdress { get; set; }
        public int NrOfRequest { get; set; }
        public int ExecutionTime { get; set; }
        public int TotalServerExecTime { get; set; }
        public Server(string name, string ipAdress)
        {
            this.Name = name;
            this.IpAdress = ipAdress;
            NrOfRequest = 0;
        }
        public Server() { }

        public void ReciveRequest(int executionTime)
        {
            NrOfRequest++;
            ExecutionTime = executionTime;
            TotalServerExecTime += executionTime;
            Console.WriteLine("Server" + Name +" got response in "+ ExecutionTime);
        }
    }
}
