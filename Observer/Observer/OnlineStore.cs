﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public class OnlineStore: IOnlineStore
    {
        public List<Product> _inventortProduct;
        private List<ISubscriber> _subscriberList;

        private double _moneyGot;

        public OnlineStore()
        {
            _inventortProduct = new List<Product>();
            _subscriberList = new List<ISubscriber>();
            _moneyGot = 0;
        }

        public bool Subscribe(ISubscriber subs)
        {
            if (!_subscriberList.Contains(subs))
            {
                _subscriberList.Add(subs);
                return true;
            }
            return false;
        }

        public bool Unsubscribe(ISubscriber subs)
        {
            if (_subscriberList.Contains(subs))
            {
                _subscriberList.Remove(subs);
                return true;
            }
            return false;
        }

        public void Notify(List<Product> productList)
        {
            _subscriberList.ForEach(a => a.Update(productList));
        }

        public void AddProducts(List<Product> productList)
        {
            productList.ForEach(a => _inventortProduct.Add(a));
            Notify(productList);
        }

        public void Inverntory()
        {
            _inventortProduct.ForEach(a => Console.WriteLine(a));
        }
    }
}
