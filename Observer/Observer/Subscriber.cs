﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public class Subscriber : ISubscriber
    {
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public void Update(List<Product> productList)
        {
            for (int i = 0; i < productList.Count; i++)
            {
                Console.WriteLine(_name + " Do you want to buy " + productList[i] + "  y/n");
                string response = Console.ReadLine();
                if (response.Equals("y"))
                {
                    productList[i].Count--;
                    Console.WriteLine("Thank you for your purchase!");
                }
            }
        }
    }
}
