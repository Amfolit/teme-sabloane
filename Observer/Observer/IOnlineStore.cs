﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public interface IOnlineStore
    {
        bool Subscribe(ISubscriber subscriber);
        bool Unsubscribe(ISubscriber subscriber);
        void Notify(List<Product> productList);
        void AddProducts(List<Product> productList);
        void Inverntory();
    }
}
