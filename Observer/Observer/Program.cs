﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    public class Program
    {
        static void Main(string[] args)
        {
            IOnlineStore store = new OnlineStore();

            ISubscriber sub = new Subscriber() { Name = "gigi" };
            ISubscriber sub2 = new Subscriber() { Name = "petre" };

            store.Subscribe(sub);
            store.Subscribe(sub2);

            Product prod = new Product() { Name = "mouse", Count = 2, Description = "E un mouse", Price = 3.5 };
            Product prod2 = new Product() { Name = "casti", Count = 5, Description = "Scoate sunete", Price = 8.5 };


            store.AddProducts(new List<Product>(){prod,prod2});

            store.Unsubscribe(sub2);

            Product prod3 = new Product() { Name = "ceva", Count = 7, Description = "ceva ceva", Price = 8.5 };
            store.Inverntory();

        }
    }
}
