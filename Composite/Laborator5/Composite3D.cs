﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class Composite3D :Entity3D
    {
        public Entity3D FirstOperand { get; set; }
        public Entity3D SecondOperand { get; set; }
        public ESolidOp Operation { get; set; }

        public override EEntityType GetType3D()
        {
            return EEntityType.Composite3D;
        }

        public ETransf TranfsType { get; set; }
        public override ETransf Transformation()
        {
            return TranfsType;
        }

    }
}
