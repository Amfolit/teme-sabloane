﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public abstract class Entity3D
    {
        public abstract EEntityType GetType3D();
        public abstract ETransf Transformation();
    }
}
