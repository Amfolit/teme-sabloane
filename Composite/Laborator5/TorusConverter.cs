﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class TorusConverter : IConverter
    {

        public Solid3D Convert(Entity3D entity3D)
        {
            Torus cub = entity3D as Torus;
            Solid3DFactory factory = new Solid3DFactory();
            return factory.CreateTorus(cub.MinorRadius,cub.MajorRadius);
        }
    }
}
