﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public interface IConverter
    {
        Solid3D Convert(Entity3D entity3D);
    }
}
