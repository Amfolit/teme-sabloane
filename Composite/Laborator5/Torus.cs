﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class Torus : Entity3D
    {
        public double MinorRadius { get; set; }
        public double MajorRadius { get; set; }

        public override EEntityType GetType3D()
        {
            return EEntityType.Torus;
        }

        public ETransf TranfsType { get; set; }
        public override ETransf Transformation()
        {
            return TranfsType;
        }
    }
}
