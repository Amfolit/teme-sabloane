﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class CubeConverter : IConverter
    {
        public Solid3D Convert(Entity3D entity3D)
        {
            Cube cub = entity3D as Cube;
            Solid3DFactory factory = new Solid3DFactory();
            return factory.CreateCube(cub.Height,cub.Lenght,cub.Width);
        }
    }
}
