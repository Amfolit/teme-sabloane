﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class Solid3D
    {
        public string Description { get; set; }

        public bool Operation3D(Entity3D entity, ESolidOp operation)
        {
            Solid3DFactory factory = new Solid3DFactory();
            //factory;
            factory.CreateCube(1, 2, 3);
            return true;
        }
        public void Render()
        {
            Console.WriteLine(Description);
        }
    }

}
