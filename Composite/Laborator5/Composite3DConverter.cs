﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class Composite3DConverter :IConverter
    {
        public Solid3D Convert(Entity3D entity3D)
        {
            Composite3D cub = entity3D as Composite3D;
            Solid3DFactory factory = new Solid3DFactory();

            Solid3D first = new Solid3D();
            Solid3D second = new Solid3D();
            switch (cub.FirstOperand.GetType3D())
            {
                case EEntityType.Cube:
                    Cube cubi = cub.FirstOperand as Cube;
                    first = factory.CreateCube(cubi.Height, cubi.Lenght, cubi.Width);
                    break;
                case EEntityType.Sphere:
                    Sphere sf = cub.FirstOperand as Sphere;
                    first = factory.CreateSphere(sf.Radius);
                    break;
                case EEntityType.Torus:
                    Torus to = cub.FirstOperand as Torus;
                    first = factory.CreateTorus(to.MinorRadius,to.MajorRadius);
                    break;
            }
            switch (cub.SecondOperand.GetType3D())
            {
                case EEntityType.Cube:
                    Cube cubi = cub.SecondOperand as Cube;
                    second = factory.CreateCube(cubi.Height, cubi.Lenght, cubi.Width);
                    break;
                case EEntityType.Sphere:
                    Sphere sf = cub.SecondOperand as Sphere;
                    second = factory.CreateSphere(sf.Radius);
                    break;
                case EEntityType.Torus:
                    Torus to = cub.SecondOperand as Torus;
                    second = factory.CreateTorus(to.MinorRadius, to.MajorRadius);
                    break;
            }
            first.Description += "   operation = " +cub.Operation+"   "+second.Description;
            return first;
        }
    }
}
