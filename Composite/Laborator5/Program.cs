﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public enum ETransf { Identity, Rotation, Scale, Translation }
    public enum ESolidOp { Union, Substraction, Intersection }
    public enum EEntityType { Cube, Sphere, Torus, Composite3D }

    public class Program
    {
        static void Main(string[] args)
        {
            Entity3D cube1 = new Cube() { Height = 2.3, Lenght = 3.5, Width = 3.0 };
            Entity3D cube2= new Cube() { Height = 3.3, Lenght = 3, Width = 1.0 };
            Entity3D sphere = new Sphere() { Radius = 6 };
            Entity3D toru = new Torus() { MajorRadius = 3,MinorRadius = 5 };

            Entity3D composite = new Composite3D() { FirstOperand = cube1,SecondOperand = sphere,Operation = ESolidOp.Union};
            ConverterSingleton converter = ConverterSingleton.GetInstance;

            Solid3D solid = converter.Convert(cube1);
            solid.Render();
            Solid3D solid2 = converter.Convert(cube2);
            solid2.Render();
            Solid3D sf = converter.Convert(sphere);
            sf.Render();
            Solid3D torus = converter.Convert(toru);
            torus.Render();

            Solid3D solidComposite = converter.Convert(composite);

            solidComposite.Render();

        }
    }
}
