﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class SphereConverter : IConverter
    {
        public Solid3D Convert(Entity3D entity3D)
        {
            Sphere cub = entity3D as Sphere;
            Solid3DFactory factory = new Solid3DFactory();
            return factory.CreateSphere(cub.Radius);
        }
    }
}
