﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class ConverterSingleton : IConverter
    {
        private static ConverterSingleton _instance;
        private Dictionary<EEntityType, IConverter> _converterMap;

        public static ConverterSingleton GetInstance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new ConverterSingleton();
                }
                return _instance;
            }
        }

        private ConverterSingleton()
        {
            _converterMap = new Dictionary<EEntityType, IConverter>();
            _converterMap.Add(EEntityType.Cube,new CubeConverter());
            _converterMap.Add(EEntityType.Composite3D, new Composite3DConverter());
            _converterMap.Add(EEntityType.Sphere, new SphereConverter());
            _converterMap.Add(EEntityType.Torus, new TorusConverter());

        }

        private IConverter GetConverter(EEntityType type)
        {
            return _converterMap[type];
        }

        public Solid3D Convert(Entity3D entity3D)
        {
            IConverter converter = GetConverter(entity3D.GetType3D());
            Solid3D solid = converter.Convert(entity3D);
            return solid;
        }
    }
}
