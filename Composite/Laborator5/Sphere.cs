﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class Sphere : Entity3D
    {
        public double Radius { get; set; }

        public override EEntityType GetType3D()
        {
            return EEntityType.Sphere;
        }

        public ETransf TranfsType { get; set; }
        public override ETransf Transformation()
        {
            return TranfsType;
        }
    }
}
