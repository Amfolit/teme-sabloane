﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class Cube:Entity3D
    {
        public double Lenght { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }

      

        public override EEntityType GetType3D()
        {
            return EEntityType.Cube;
        }

        public ETransf TranfsType { get; set; }
        public override ETransf Transformation()
        {
            return TranfsType;
        }
    }
}
