﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator5
{
    public class Solid3DFactory
    {
        public Solid3D CreateCube(double x, double y, double z)
        {
            Solid3D solid = new Solid3D();

            solid.Description = "This is a Cube w="+x+" l="+y+" h="+z;
            return solid;
        }
        public Solid3D CreateSphere(double x)
        {
            Solid3D solid = new Solid3D();

            solid.Description = "This is a Sphere r="+x;
            return solid;
        }
        public Solid3D CreateTorus(double x, double y)
        {
            Solid3D solid = new Solid3D();

            solid.Description = "This is a Torus  "+x+ "  " + y;
            return solid;
        }
    }
}
