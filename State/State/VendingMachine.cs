﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    public class VendingMachine
    {
        public int Capacity { get; set; }
        public State MachineState { get; set; }

        public SoldState soldState { get; set; }
        public NoCoinState noCoinState { get; set; }
        public SoldOutState soldOutState { get; set; }
        public HasCoinState hasCoinState { get; set; }

        public VendingMachine()
        {
            Capacity = 2;
            soldState = new SoldState();
            noCoinState = new NoCoinState();
            soldOutState = new SoldOutState();
            hasCoinState = new HasCoinState();
            MachineState = noCoinState;
        }

        public void UpdateState(EUserOperation option)
        {
            switch (option)
            {
                case EUserOperation.InsertCoin:
                    {
                        if (!InsertCoin())
                            Console.WriteLine("Can't insert");
                        break;
                    }
                case EUserOperation.EjectCoin:
                    {
                        if (!EjectCoin())
                            Console.WriteLine("Can't Eject");
                        break;
                    }
                case EUserOperation.BuyProduct:
                    {
                        if (!BuyProduct())
                            Console.WriteLine("Can't buy");
                        break;
                    }
                case EUserOperation.FillMachine:
                    {
                        ReFill();
                        break;
                    }
                case EUserOperation.InspectMachine:
                    {
                        Inspect();
                        break;
                    }
                case EUserOperation.Exit:
                    break;
                default:
                    break;
            }

        }

        private void ReFill()
        {
            Console.WriteLine("Macine Has been filled");
            Capacity = 2;
            SetMachineState(noCoinState);
        }

        private bool InsertCoin()
        {
            if (MachineState.InsertCoin())
            {
                Console.WriteLine("Inserting coin");
                SetMachineState(hasCoinState);
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool EjectCoin()
        {
            if (MachineState.EjectCoin())
            {
                Console.WriteLine("Ejecting coin");
                SetMachineState(noCoinState);
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool BuyProduct()
        {
            if (MachineState.BuyProduct())
            {
                Capacity--;
                Console.WriteLine("Buying product");
                if (Capacity == 0)
                    SetMachineState(soldOutState);
                else 
                    SetMachineState(soldState);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Inspect()
        {
            Console.WriteLine(MachineState.GetType() + " " + Capacity);
        }

        public bool IsEmpty()
        {
            if (Capacity == 0)
                return true;
            return false;
        }

        public void SetMachineState(State state)
        {
            MachineState = state;
        }

    }
}
