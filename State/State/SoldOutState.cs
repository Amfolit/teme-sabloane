﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    public class SoldOutState : State
    {
        public override bool InsertCoin()
        {
            return false;
        }

        public override bool EjectCoin()
        {
            return false;
        }

        public override bool BuyProduct()
        {
            return false;
        }

        public override bool Dispence()
        {
            return false;
        }
    }
}
