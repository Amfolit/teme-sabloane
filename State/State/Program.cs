﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    public enum EUserOperation { InsertCoin, EjectCoin, BuyProduct, FillMachine, InspectMachine, Exit }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choose");
            String command = Console.ReadLine();
            VendingMachine vm = new VendingMachine();

            while (!command.Equals("exit"))
            {
                switch (command)
                {
                    case "insert":
                        vm.UpdateState(EUserOperation.InsertCoin);
                        break;
                    case "eject":
                        vm.UpdateState(EUserOperation.EjectCoin);
                        break;
                    case "buy":
                        vm.UpdateState(EUserOperation.BuyProduct);
                        break;
                    case "fill":
                        vm.UpdateState(EUserOperation.FillMachine);
                        break;
                    case "inspect":
                        vm.UpdateState(EUserOperation.InspectMachine);
                        break;
                    default:
                        break;
                }
                command = Console.ReadLine();
            }
        }
    }
}
