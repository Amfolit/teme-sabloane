﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator4
{
    public class BasicCar : ICar
    {
        public int Color { get; set; }
        public string Engine { get; set; }
        public string Transmision { get; set; }
        public int Price { get; set; }
        public List<string> AccesorieList;
        public ECarType CarType { get; set; }

        public BasicCar()
        {
            Price = 0;
            AccesorieList = new List<string>();
        }


        public void SetAccesory()
        {
        }

        public void Assemble()
        {
            Price = 7800;
        }

        public void Display()
        {
            Console.WriteLine(Price);
        }

        public ECarType GetCarType()
        {
            return ECarType.Basic;
        }
    }
}
