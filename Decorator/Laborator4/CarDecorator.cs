﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator4
{
    public abstract class CarDecorator : ICar
    {
        protected BasicCar originalCar;

        public CarDecorator(BasicCar car)
        {
            originalCar = car;
        }

        virtual public void SetAccesory(){}

        virtual public void Assemble(){}

        virtual public void Display(){}

        virtual public ECarType GetCarType() { return ECarType.Basic;  }
    }
}
