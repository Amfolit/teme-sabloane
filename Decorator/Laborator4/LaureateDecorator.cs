﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator4
{
    public class LaureateDecorator: AmbientDecorator
    {
        public LaureateDecorator(BasicCar car)
            : base(car)
        {
        }

        public override void Assemble()
        {
            base.Assemble();
            SetAccesory();
            originalCar.CarType = GetCarType();
        }

        public override void Display()
        {
            base.Display();
        }

        public override void SetAccesory()
        {
            originalCar.AccesorieList.Add("Aer Conditionat");
            originalCar.Price += 470;
            originalCar.AccesorieList.Add("Senzor temperatura");
            originalCar.Price += 15;
            originalCar.AccesorieList.Add("Computer bord");
            originalCar.Price += 300;
            originalCar.AccesorieList.Add("Scaun sofer reglabil");
            originalCar.Price += 45;
            originalCar.AccesorieList.Add("Ceve");
            originalCar.Price += 170;

        }
        public override ECarType GetCarType()
        {
            return ECarType.Laureate;
        }
    }
}
