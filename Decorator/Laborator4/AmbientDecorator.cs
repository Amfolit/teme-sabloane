﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator4
{
    public class AmbientDecorator : AccesDecorator
    {
        public AmbientDecorator(BasicCar car)
            : base(car)
        {
        }

        public override void Assemble()
        {
            base.Assemble();
            SetAccesory();
                                                                                                                                            originalCar.Price -= 300;
            originalCar.CarType = GetCarType();
        }

        public override void Display()
        {
            base.Display();
        }

        public override void SetAccesory()
        {
            originalCar.AccesorieList.Add("Centuri");
            originalCar.Price += 400;
            originalCar.AccesorieList.Add("Tetiere");
            originalCar.Price += 150;
            originalCar.AccesorieList.Add("Asistenta la franare");
            originalCar.Price += 50;
            originalCar.AccesorieList.Add("Volan Reglabil");
            originalCar.Price += 300;
            originalCar.AccesorieList.Add("Centuri siguante fixe");
            originalCar.Price += 100;
        }
        public override ECarType GetCarType()
        {
            return ECarType.Ambiance;
        }
    }
}
