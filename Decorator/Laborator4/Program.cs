﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator4
{
    class Program
    {
        static void Main(string[] args)
        {
            BasicCar car = new BasicCar();
            car.Assemble();
            car.Display();
            Console.WriteLine(car.GetCarType());
            Console.WriteLine();

            AccesDecorator ac = new AccesDecorator(car);
            ac.Assemble();
            ac.Display();
            Console.WriteLine(ac.GetCarType());
            Console.WriteLine();
            car = new BasicCar();

            AmbientDecorator am = new AmbientDecorator(car);
            am.Assemble();
            am.Display();
            Console.WriteLine(am.GetCarType());
            Console.WriteLine();

            car = new BasicCar();
            LaureateDecorator la = new LaureateDecorator(car);
            la.Assemble();
            la.Display();
            Console.WriteLine(la.GetCarType());
            Console.WriteLine();

        }
    }
}
