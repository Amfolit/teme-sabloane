﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator4
{
    public class AccesDecorator:CarDecorator
    {
        public AccesDecorator(BasicCar car): base(car)
        {
        }

        public override void Assemble()
        {
            originalCar.Assemble();
            SetAccesory();
            originalCar.CarType = GetCarType();
        }

        public override void Display()
        {
            originalCar.Display();
        }

        public override void SetAccesory()
        {
            originalCar.AccesorieList.Add("Sistem de incalzire-ventilare");
            originalCar.Price += 50;
            originalCar.AccesorieList.Add("Directie Asistata");
            originalCar.Price += 150;
            originalCar.AccesorieList.Add("ABS");
            originalCar.Price += 130;
            originalCar.AccesorieList.Add("ABS");
            originalCar.Price += 370;

        }
        public override ECarType GetCarType()
        {
            return ECarType.Acces;
        }
    }
}
