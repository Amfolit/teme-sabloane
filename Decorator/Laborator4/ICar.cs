﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator4
{
    public enum ECarType { Basic, Acces, Ambiance, Laureate }

    public interface ICar
    {
        ECarType GetCarType();
        void SetAccesory();
        void Assemble();
        void Display();
    }
}
