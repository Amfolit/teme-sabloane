﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public class Truck : Car
    {
        public override string ToString()
        {
            return "Truck "+ this.ID +" "+this.Color+" "+this.Brand+" "+this.Price;
        }
    }
}
