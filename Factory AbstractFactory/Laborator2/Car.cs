﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public enum CarBrand { Mercedez_Benz, Volvo, Renault, Volkswagen }

    public abstract class Car
    {
        public int ID { get; set; }
        public string Color { get; set; }
        public int Price { get; set; }
        public CarBrand Brand { get; set; }

    }
}
