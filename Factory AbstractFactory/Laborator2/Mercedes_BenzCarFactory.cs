﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public class Mercedes_BenzCarFactory : AbstractCarFactory
    {
        public override Car GetAutomobile(string color, int id, int price)
        {
            return new Automobil() { Price = price,ID =id,Color = color,Brand = CarBrand.Mercedez_Benz};
        }
        public override Car GetTruck(string color, int id, int price)
        {
            return new Truck() { Price = price, ID = id, Color = color, Brand = CarBrand.Mercedez_Benz };
        }
    }
}
