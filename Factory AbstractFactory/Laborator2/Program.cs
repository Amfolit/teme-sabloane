﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dealer dealer = new Dealer();

            dealer.OrderCar(Dealer.CarType.Automobil, CarBrand.Mercedez_Benz, "red", 100);
            dealer.OrderCar(Dealer.CarType.Truck, CarBrand.Volvo, "blue", 1100);
            dealer.OrderCar(Dealer.CarType.Automobil, CarBrand.Volkswagen, "grey", 10012);
            dealer.OrderCar(Dealer.CarType.Truck, CarBrand.Mercedez_Benz, "fucsia", 10540);

            dealer.SellCar(1);

            Console.WriteLine();
            Console.WriteLine();

            dealer.GetAllCars();

            Console.WriteLine();
            Console.WriteLine();

            dealer.TestCar(2);
        }
    }
}
