﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public interface ICarFactory
    {
        Car GetCar(CarBrand carBrand, string color, int id,int price);
    }
}
