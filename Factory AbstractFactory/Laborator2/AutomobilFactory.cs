﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public class AutomobilFactory : ICarFactory
    {
        public AutomobilFactory()
        {
        }
        public Car GetCar(CarBrand carBrand, string color, int id, int price)
        {
            Car NewCar = new Automobil() { ID = id, Color = color, Brand = carBrand, Price = price };
            return NewCar;
        }
    }
}
