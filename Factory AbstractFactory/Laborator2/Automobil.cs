﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public class Automobil : Car
    {
        public override string ToString()
        {
            return "Automobil " + this.ID + " " + this.Color + " " + this.Brand + " " + this.Price;
        }
    }
}
