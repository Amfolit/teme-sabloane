﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public class TruckFactory : ICarFactory
    {
        public TruckFactory()
        {  
        }
        public Car GetCar(CarBrand carBrand, string color, int id,int price)
        {
            Car NewCar = new Truck() { ID = id, Color = color, Brand = carBrand, Price = price };
            return NewCar;
        }
    }
}
