﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public abstract class AbstractCarFactory
    {
        public abstract Car GetAutomobile(string color, int id, int price);
        public abstract Car GetTruck(string color, int id, int price);
    }
}
