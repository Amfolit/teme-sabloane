﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laborator2
{
    public class Dealer
    {
        public enum CarType { Automobil, Truck }

        private readonly bool AstractFactorySwitch = true;

        public static int id = 1;
        ICarFactory Factory = null;
        public AbstractCarFactory AbsFactory = null;
        public List<Car> CarList;

        public Dealer()
        {
            CarList = new List<Car>();
        }

        public Car OrderCar(CarType carType,CarBrand carBrand,string color,int price)
        {
            if (!AstractFactorySwitch)
            {
                switch (carType)
                {
                    case CarType.Automobil:
                        Factory = new AutomobilFactory();
                        break;
                    case CarType.Truck:
                        Factory = new TruckFactory();
                        break;
                }
                Car oredredCar = Factory.GetCar(carBrand, color,id, price);
                id++;
                CarList.Add(oredredCar);
                return oredredCar;
            }
            else
            {
                switch (carBrand)
                {
                    case CarBrand.Mercedez_Benz:
                        AbsFactory = new Mercedes_BenzCarFactory();
                        break;
                    case CarBrand.Renault:
                        AbsFactory = new RenaultCarFactory();
                        break;
                    case CarBrand.Volvo:
                        AbsFactory = new VolvoCarFactory();
                        break;
                    case CarBrand.Volkswagen:
                        AbsFactory = new VolksswagenCarFactory();
                        break;
                }
                Car newCar = null;
                switch (carType)
                {
                    case CarType.Automobil:
                        newCar = AbsFactory.GetAutomobile(color, id, price);
                        break;
                    case CarType.Truck:
                        newCar = AbsFactory.GetTruck(color,id,price);
                        break;
                }
                CarList.Add(newCar);
                return newCar;
            }
        }

        public void SellCar(int id)
        {
            Car selledCar = CarList.Where(a => a.ID == id).FirstOrDefault();
            if (selledCar != null)
            {
                Console.WriteLine("Car : " + selledCar + " HAS BEEN SOLD $$$$");
                CarList.Remove(selledCar);
            }
            else
            {
                Console.WriteLine("Car doese not exist!");
            }
        }

        public void GetAllCars()
        {
            CarList.ForEach(a => Console.WriteLine(a));
        }

        public void TestCar(int id)
        {
            Car sellectedCar = CarList.Where(a => a.ID == id).FirstOrDefault();
            if (sellectedCar != null)
            {
                Random rand = new Random();
                Console.WriteLine("The Car :" + sellectedCar + " was tested for : " + rand.Next(10, 1000) + " km");
            }
            else
            {
                Console.WriteLine("Inexisting Car");
            }
        }
    }
}
