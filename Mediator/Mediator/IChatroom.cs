﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    public interface IChatroom
    {
        bool Register(IUser user);
        bool Unregister(IUser user);
        void Send(string message, IUser sender);
    }
}
