﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    public class User : IUser
    {
        public string Name { get; set; }
        public IChatroom Chatroom { get; set; }

        public User(string name)
        {
            Name = name;
        }

        public override bool Equals(object obj)
        {
            if (this.Name.Equals(((IUser)obj).Name))
                return true;
            return false;
        }

        public override string ToString()
        {
            return Name;
        }

        public void Send(string message)
        {
            if (Chatroom != null)
                Chatroom.Send(message, this);
            else 
            Console.WriteLine(Name + "Not in a chatroom");
        }

        public void Recive(IUser sender, string message)
        {
            Console.WriteLine(Name + " Recived : " + message + " from :" + sender);
        }
    }
}
