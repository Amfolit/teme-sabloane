﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    public class Program
    {
        static void Main(string[] args)
        {
            IChatroom chatroom = new Chatroom();

            IUser user1 = new User("gigi");
            IUser user2 = new User("frone");
            IUser user3 = new User("becali");

            chatroom.Register(user1);
            chatroom.Register(user2);
            chatroom.Register(user3);

            user1.Send("salut!");

            chatroom.Unregister(user3);

            user2.Send("noroc si tie!");
        }
    }
}
