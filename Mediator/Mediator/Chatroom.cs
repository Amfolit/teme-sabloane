﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    public class Chatroom : IChatroom
    {
        private List<IUser> userList;

        public Chatroom()
        {
            userList = new List<IUser>();
        }

        public bool Register(IUser user)
        {
            if (!userList.Contains(user))
            {
                userList.Add(user);
                user.Chatroom = this;
                return true;
            }
            else
            {
                Console.WriteLine("User allready in");
                return false;
            }
        }

        public bool Unregister(IUser user)
        {
            if (userList.Contains(user))
            {
                userList.Remove(user);
                user.Chatroom = null;
                return true;
            }
            else
            {
                Console.WriteLine("User not in");
                return false;
            }
        }

        public void Send(string message, IUser sender)
        {
            foreach (IUser users in userList)
            {
                if (users.Equals(sender))
                    continue;
                users.Recive(sender, message);
            }
        }
    }
}
