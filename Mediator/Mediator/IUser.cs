﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    public interface IUser
    {
        string Name { get; set; }
        IChatroom Chatroom { get; set; }

        void Send(string message);
        void Recive(IUser sender, string message);
    }
}
