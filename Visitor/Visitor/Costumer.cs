﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    public class Costumer: IVisitable
    {
        List<Order> _orderList;
        public string Name { get; set; }

        public Costumer()
        {
            _orderList = new List<Order>();
        }

        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
            foreach (var order in _orderList)
            {
                order.Accept(visitor);
            }
        }

        public void Add(Order order)
        {
            _orderList.Add(order);
        }
    }
}
