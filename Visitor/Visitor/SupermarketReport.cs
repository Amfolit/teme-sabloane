﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    public class SupermarketReport: IVisitor
    {
        public int iProductNo { get; set; }
        public int iOrderNo { get; set; }
        public int iCustomerNo { get; set; }
        public double dTotalPrice { get; set; }

        public SupermarketReport()
        {
            iProductNo = 0;
            iOrderNo = 0;
            iCustomerNo = 0;
            dTotalPrice = 0.0;
        }
        public void Visit(Product prod)
        {
            iProductNo++;
            dTotalPrice += prod.Price;
        }

        public void Visit(Order order)
        {
            iOrderNo++;
        }

        public void Visit(Costumer cust)
        {
            iCustomerNo++;
        }

        public void DisplayResult()
        {
            Console.WriteLine("Today we had " + iCustomerNo + " customers");
            Console.WriteLine("Today we had " + iOrderNo + " orders places");
            Console.WriteLine("Today we had " + iProductNo + " products sold");
            Console.WriteLine("Today's profit $$$$ = " + dTotalPrice + " $$$$");
        }
    }
}
