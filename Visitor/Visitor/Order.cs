﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    public class Order: IVisitable
    {
        private List<Product> _productList;
        public string Name { get; set; }

        public Order()
        {
            _productList = new List<Product>();
        }
        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
            foreach (var prod in _productList)
            {
                prod.Accept(visitor);
            }
        }

        public void Add(Product prod)
        {
            _productList.Add(prod);
        }
    }
}
