﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    public interface IVisitor
    {
        void Visit(Product prod);
        void Visit(Order order);
        void Visit(Costumer cust);
        void DisplayResult();
    }
}
