﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor
{
    public class Program
    {
        static void Main(string[] args)
        {
            IVisitor vist = new SupermarketReport();

            Costumer c1 = new Costumer() { Name = "gigi" };
            Order o1 = new Order() { Name = "aloha" };
            c1.Add(o1);
            o1.Add(new Product() { Name = "", Price = 5.5 });
            o1.Add(new Product() { Name = "", Price = 7.9 });
            o1.Add(new Product() { Name = "", Price = 3 });
            o1.Add(new Product() { Name = "", Price = 0.1 });

            c1.Accept(vist);

            vist.DisplayResult();
            
        }
    }
}
