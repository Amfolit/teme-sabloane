﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ChainOfResponsability
{
    public class VacationRequest
    {
        public int LastRequestNumber { get; set; }
        public DateTime StartDay { get; set; }
        public DateTime EndDay { get; set; }
        public int RequestNumber { get; set; }

        public VacationRequest(DateTime start,DateTime end)
        {
            StartDay = start;
            EndDay = end;
        }

        public int GetNumberOfDays()
        {
            return Math.Abs( (int)(StartDay - EndDay).TotalDays );
        }
    }
}
