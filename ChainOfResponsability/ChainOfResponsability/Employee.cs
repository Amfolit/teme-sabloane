﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability
{
    public abstract class Employee
    {
        public Employee Supervisor;

        public Employee()
        {

        }


        public void ApplyVacation(VacationRequest vacay)
        {
            if (this.GetType() != Supervisor.GetType())
            {
                if (ApproveVacation(vacay))
                {
                    Console.WriteLine("Vacation aproved  by " + GetType() + " for " + vacay.GetNumberOfDays());
                }
                else
                {
                    if (Supervisor != null)
                    {
                        Supervisor.ApplyVacation(vacay);
                    }
                    else
                    {
                        Console.WriteLine("Nope");
                    }
                }
            }
            else
                Console.WriteLine("Can't aprove for self");
        }

        protected bool ApproveVacation(VacationRequest vacay)
        {
            if (GetMaxVacationDayCanAprove() > vacay.GetNumberOfDays())
                return true;
            return false;
        }

        public abstract int GetMaxVacationDayCanAprove();

    }
}
