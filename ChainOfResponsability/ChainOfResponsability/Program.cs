﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability
{
    public class Program
    {
        static void Main(string[] args)
        {
            VacationRequest request = new VacationRequest(DateTime.Now, new DateTime(2017,4,19)); 


            Employee dev = new Developer();
            Employee teamLeader = new TeamLeader();
            dev.Supervisor = teamLeader;
            Employee projectManager = new ProjectLeader();
            teamLeader.Supervisor = projectManager;
            Employee departmentDirector = new DepartmentDirector();
            projectManager.Supervisor = departmentDirector;


            dev.ApplyVacation(request);
        }
    }
}
