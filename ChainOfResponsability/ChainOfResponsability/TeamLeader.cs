﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsability
{
    public class TeamLeader : Employee
    {
        public override int GetMaxVacationDayCanAprove()
        {
            return 5;
        } 
    }
}
